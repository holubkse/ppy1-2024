import json, os
from datetime import datetime
from datetime import timezone
import DBOperations as dbo

dirname = os.path.dirname(__file__)
DATABASE_FILE = os.path.join(dirname, "../../db.sqlite3")
SNAP_DATA = os.path.join(dirname, "../data/snap.dat")
JSON_DATA = os.path.join(dirname, "../data/registeredUsers.json")

def getCurrentTime() -> str:
    return str(datetime.now(timezone.utc))

def getUserAmount() -> str:
    conn = dbo.connect(DATABASE_FILE)
    return str(dbo.countTableRows(conn, "main", "fileshare_fileuser"))

def getSnapshotNum() -> int:
    with open(SNAP_DATA, "r") as f:
        ret = f.read().split()[0]
        newNum = int(ret) + 1
        f.close()
    
    with open(SNAP_DATA, "w") as f:
        f.write(str(newNum))
        f.close()
    
    return int(ret)

def initSnapshot() -> None:
    currentTime = getCurrentTime()
    userAmount = getUserAmount()
    ret = {
        f"snapshot{getSnapshotNum()}" : {
            "time" : currentTime,
            "users": userAmount
        }
    }

    with open(JSON_DATA, "w") as outFile:
        json.dump(ret, outFile, indent=4)
        outFile.close()

def createSnapshot():
    with open(JSON_DATA, "r+") as f:
        readData = f.read()
        if not readData:
            raise Exception("Json file is empty, cannot create snapshot")
        
        currentTime = getCurrentTime()
        userAmount = getUserAmount()
        ret = {
            f"snapshot{getSnapshotNum()}" : {
                "time" : currentTime,
                "users": userAmount
            }
        }
        
        loadData = json.loads(readData)
        loadData.update(ret)
        f.close()

    with open(JSON_DATA, "w") as f:
        json.dump(loadData, f, indent=2, ensure_ascii=True)
        f.close()

def getSnapshots() -> dict:
    with open(JSON_DATA, "r") as f:
        readData = f.read()
        snapshots = json.loads(readData)
    f.close()

    return snapshots

if __name__ == "__main__":
    initSnapshot()
