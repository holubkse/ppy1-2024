import unittest
from snake import Snake
from food import FoodFactory, Food

class TestCollision(unittest.TestCase):
    
    def setUp(self) -> None:
        """
        Příprava prostředí pro testy. Vytváří nové instance hada a továrny na jídlo.
        """

        self.snake = Snake()
        self.food_factory = FoodFactory()

    def test_collision_with_itself(self) -> None:
        """
        Testuje, zda had koliduje sám se sebou.

        Scénář:
            1. Nastavíme hada tak, že se jeho tělo překrývá.
            2. Zkontrolujeme, zda metoda check_collision vrátí True.

        Očekávaný výsledek:
            Had by měl kolidovat sám se sebou.
        """

        self.snake.positions = [(14, 14), (15, 14), (14, 14)]  # Nastavení hada tak, aby kolidoval
        self.assertTrue(self.snake.check_collision(), "Had by měl kolidovat sám se sebou")

    def test_snake_collides_with_food(self) -> None:
        """
        Testuje, zda had správně konzumuje jídlo a zda se jídlo odstraní.

        Scénář:
            1. Nastavíme hada a jídlo na stejnou pozici.
            2. Simulujeme konzumaci jídla hadem.
            3. Zkontrolujeme, zda bylo jídlo odstraněno ze seznamu.

        Očekávaný výsledek:
            Jídlo by mělo být odstraněno po konzumaci hadem.
        """

        self.snake.positions = [(10, 10)]  # Nastavení hada na pozici jídla
        food = Food()
        food.position = (10, 10)
        self.food_factory.food_list = [food]

        # Simulace konzumace jídla hadem
        if self.snake.positions[0] == food.position:
            self.food_factory.remove_food(food)

        self.assertEqual(len(self.food_factory.food_list), 0, "Jídlo by mělo být odstraněno po konzumaci hadem")

if __name__ == '__main__':
    unittest.main()

