"""
Hlavní skript pro spuštění hry Snake.

Tento skript inicializuje herní prostředí, spravuje hlavní herní smyčku, zpracovává vstupy od uživatele, vykresluje herní objekty a zajišťuje herní logiku.

Funkce:
    main() -> None:
        Spustí herní smyčku, která zpracovává události, aktualizuje stav hry, vykresluje objekty a zajišťuje interakce mezi objekty.
"""

import pygame
from settings import *  # Importování konstant pro nastavení okna a hry
from snake import Snake
from food import FoodFactory
from utils import draw_snake, draw_text

def main():
    """
    Spustí herní smyčku, která zpracovává události, aktualizuje stav hry, vykresluje objekty a zajišťuje interakce mezi objekty.

    Tento skript:
    - Inicializuje Pygame a nastaví herní okno.
    - Vytvoří instance hada a továrny na jídlo.
    - Zpracovává vstupy od uživatele pro změnu směru pohybu hada.
    - Aktualizuje a vykresluje pozice hada a jídla.
    - Zkontroluje kolize a aktualizuje skóre.
    - Zobrazí výsledkovou obrazovku při konci hry.

    Vedlejší účinky:
        - Zobrazí herní okno a vykreslí herní objekty.
        - Reaguje na vstupy od uživatele a aktualizuje herní stav.
        - Zobrazí výsledkovou obrazovku při konci hry.

    Vstupy:
        - Uživatel může použít klávesy šipek (nahoru, dolů, vlevo, vpravo) k řízení hada.
    """

    pygame.init()
    clock = pygame.time.Clock()
    screen = pygame.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT))
    pygame.display.set_caption('Snake Game')

    snake = Snake()
    food_factory = FoodFactory(max_food=5)
    score = 0

    key_direction_map = {
        pygame.K_UP: (0, -1),
        pygame.K_DOWN: (0, 1),
        pygame.K_LEFT: (-1, 0),
        pygame.K_RIGHT: (1, 0)
    }

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                return
            elif event.type == pygame.KEYDOWN:
                if event.key in key_direction_map:
                    snake.queue_direction(key_direction_map[event.key])

        snake.move()

        if snake.check_collision():
            break

        for food in food_factory.food_list:
            if snake.positions[0] == food.position:
                snake.grow_snake()
                score += 1
                food_factory.remove_food(food)

        food_factory.update()

        screen.fill(BLACK)
        draw_snake(screen, snake)
        food_factory.draw_food(screen)
        draw_text(screen, f"Score: {score}", 36, WHITE, (WINDOW_WIDTH // 2, 20))
        pygame.display.flip()

        clock.tick(FPS)

    screen.fill(BLACK)
    draw_text(screen, "GAME OVER", 72, RED, (WINDOW_WIDTH // 2, WINDOW_HEIGHT // 2))
    draw_text(screen, f"Final Score: {score}", 36, WHITE, (WINDOW_WIDTH // 2, WINDOW_HEIGHT // 2 + 50))
    pygame.display.flip()
    pygame.time.wait(3000)

if __name__ == "__main__":
    main()

