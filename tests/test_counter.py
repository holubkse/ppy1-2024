import sys
from os import path
path_to_src = path.join(sys.path[0], '..', 'src')
sys.path.append(path_to_src)

import hypothesis.strategies as st
from hypothesis import given
from counter import count

# Define a strategy
the_strings = st.text(alphabet='abcdefg ', min_size=1, max_size=10)

# Test the counter function using Hypothesis
@given(the_strings)
def test_counter(s):
    assert count(s) == len(s)
